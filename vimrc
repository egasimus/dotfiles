set nocompatible
filetype off


" OMG PLUGINS
set rtp+=~/.vim/bundle/Vundle.vim/
call vundle#begin()
Plugin 'gmarik/Vundle.vim'

Plugin 'vim-scripts/VimClojure'
Plugin 'digitaltoad/vim-jade'
Plugin 'wavded/vim-stylus'
Plugin 'plasticboy/vim-markdown'
Plugin 'rodjek/vim-puppet'

Plugin 'henrik/vim-indexed-search'
"Plugin 'ns9tks/vim-l9/'
"Plugin 'othree/vim-autocomplpop'
Plugin 'jpalardy/vim-slime'
" Plugin 'Shougo/neocomplete'
" Plugin 'wincent/command-t'
Plugin 'jlanzarotta/bufexplorer'
Plugin 'lyokha/vim-xkbswitch'
Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/nerdcommenter'
Plugin 'scrooloose/syntastic'
Plugin 'majutsushi/tagbar'
Plugin 'jistr/vim-nerdtree-tabs'
Plugin 'tpope/vim-fugitive'
Plugin 'airblade/vim-gitgutter'
Plugin 'bling/vim-airline'
Plugin 'Lokaltog/vim-easymotion'
Plugin 'terryma/vim-multiple-cursors'

call vundle#end()
filetype plugin on
syntax on


"   Clojure
let g:vimclojure#HighlightBuiltins = 1
let g:vimclojure#ParenRainbow = 1


"   Command-T (disabled)
" nnoremap <Leader>c :CommandT<CR>


"   Xkbswitch
let g:XkbSwitchEnabled = 1


"   vim-slime
let g:slime_target = "tmux"
let g:slime_default_config = {"socket_name": "default", "target_pane": "1"}


"   NERDTree
let g:NERDTreeWinPos = "left"
let g:NERDTreeIgnore = ['\.pyc$']
map  <F3> <plug>NERDTreeTabsToggle<CR>
"imap <F3> <Esc><plug>NERDTreeTabsToggle<plug>NERDTreeFocusToggle<CR>i
"map <Leader>t <plug>NERDTreeFocusToggle<CR>
"map <Leader>T <plug>NERDTreeTabsToggle<CR>


"   Syntastic
map <silent> <Leader>e :Errors<CR>
map <Leader>s :SyntasticToggleMode<CR>
let g:syntastic_auto_loc_list=0
let g:syntastic_loc_list_height=5
map <silent> tu :call GHC_BrowseAll()<CR>
map <silent> tw :call GHC_ShowType(1)<CR>


"   Tagbar
nmap <F8> :TagbarToggle<CR>
let g:tagbar_autofocus = 1


"   GitGutter
map <Leader>g :GitGutterToggle<CR>
let g:gitgutter_sign_added = "+ "
let g:gitgutter_sign_modified = "* "
let g:gitgutter_sign_removed = "- "
let g:gitgutter_sign_removed_first_line = "^ "
let g:gitgutter_sign_modified_removed = "~ "


"   NeoComplete
let g:acp_enableAtStartup = 0
let g:neocomplete#enable_at_startup = 0
let g:neocomplete#enable_smart_case = 1
let g:neocomplete#sources#syntax#min_keyword_length = 3
let g:neocomplete#lock_buffer_name_pattern = '\*ku\*'
let g:neocomplete#sources#dictionary#dictionaries = {
    \ 'default'  : '',
    \ 'vimshell' : $HOME.'/.vimshell_hist' }
if !exists('g:neocomplete#keyword_patterns')
    let g:neocomplete#keyword_patterns = {}
endif
let g:neocomplete#keyword_patterns['default'] = '\h\w*'
autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
    map <silent> tu :call GHC_BrowseAll()<CR>
autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags
if !exists('g:neocomplete#sources#omni#input_patterns')
  let g:neocomplete#sources#omni#input_patterns = {}
endif
"let g:neocomplete#enable_cursor_hold_i = 1
"let g:neocomplete#enable_insert_char_pre = 1
"let g:neocomplete#enable_auto_select = 1

"inoremap <expr><C-g> neocomplete#undo_completion()
"inoremap <expr><C-l> neocomplete#complete_common_string()
"inoremap <silent> <CR> <C-r>=<SID>my_cr_function()<CR>
"function! s:my_cr_function()
  "return neocomplete#close_popup() . "\<CR>"
  ""return pumvisible() ? neocomplete#close_popup() : "\<CR>"
"endfunction

"inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"
"inoremap <expr><C-h>  neocomplete#smart_close_popup()."\<C-h>"
"inoremap <expr><BS>   neocomplete#smart_close_popup()."\<C-h>"
"inoremap <expr><C-y>  neocomplete#close_popup()
"inoremap <expr><C-e>  neocomplete#cancel_popup()
""inoremap <expr><Space> pumvisible() ? neocomplete#close_popup() : "\<Space>"
"inoremap <expr><Left>  neocomplete#close_popup() . "\<Left>"
"inoremap <expr><Right> neocomplete#close_popup() . "\<Right>"
"inoremap <expr><Up>    neocomplete#close_popup() . "\<Up>"
"inoremap <expr><Down>  neocomplete#close_popup() . "\<Down>"


" Airline
let g:airline_powerline_fonts = 1


" Misc options
nnoremap ; :
set mouse=a
" set ttymouse=urxvt  " Work past ~220th column
set tabstop=2
set softtabstop=2
set shiftwidth=2
set expandtab
" set cc=80
set foldmethod=indent
set foldnestmax=2
set foldlevelstart=99
nnoremap <space> za
vnoremap <space> zf
set nonumber
map <Leader>n :set nonumber!<CR>
set nowrap
" set paste  " breaks neocomplete?!
set pastetoggle=<F2>
set wildmode=longest,list,full
set wildmenu
set laststatus=2
set shell=/usr/bin/zsh
cmap w!! %!sudo tee > /dev/null %

"   Search
set incsearch
set ignorecase
set smartcase
set hlsearch
nmap \h :nohlsearch<CR>


"   Highlight lines longer than 80 chars
nnoremap <silent> <Leader>l
      \ :if exists('w:long_line_match') <Bar>
      \   silent! call matchdelete(w:long_line_match) <Bar>
      \   unlet w:long_line_match <Bar>
      \ elseif &textwidth > 0 <Bar>
      \   let w:long_line_match = matchadd('ErrorMsg', '\%>'.&tw.'v.\+', -1) <Bar>
      \ else <Bar>
      \   let w:long_line_match = matchadd('ErrorMsg', '\%>80v.\+', -1) <Bar>
      \ endif<CR>


"   Colors!
hi ColorColumn ctermbg=0
hi TabLine     ctermfg=0             term=bold  cterm=bold
hi TabLineSel  ctermfg=7  ctermbg=10 term=bold  cterm=bold 
hi TabLineFill ctermbg=0  ctermbg=10 term=bold  cterm=bold
hi VertSplit   ctermbg=0  ctermfg=16
hi Folded      ctermbg=0  ctermfg=DarkGray
hi Pmenu       ctermbg=8  guibg=#606060
hi PmenuSel    ctermbg=3  ctermfg=0
hi PmenuSbar   ctermbg=11 ctermfg=0

highlight SignColumn            ctermbg=0 ctermfg=0
highlight GitGutterAdd          ctermbg=0 ctermfg=2
highlight GitGutterChange       ctermbg=0 ctermfg=3
highlight GitGutterDelete       ctermbg=0 ctermfg=1
highlight GitGutterChangeDelete ctermbg=0 ctermfg=4
"highlight GitGutterAdd ctermbg=2 ctermfg=0
"highlight GitGutterChange ctermbg=3 ctermfg=0
"highlight GitGutterDelete ctermbg=1 ctermfg=0
"highlight GitGutterChangeDelete ctermbg=4 ctermfg=0


filetype indent off  " Begone I say!
