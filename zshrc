# https://github.com/robbyrussell/oh-my-zsh
export ZSH=$HOME/.oh-my-zsh
ZSH_THEME="blinks"
HIST_STAMPS="yyyy-mm-dd"
plugins=(cabal git virtualenvwrapper zsh-syntax-highlighting)
source $ZSH/oh-my-zsh.sh

# https://github.com/tarruda/zsh-autosuggestions
#source ~/.oh-my-zsh/custom/plugins/zsh-autosuggestions/autosuggestions.zsh
#zle-line-init() {
    #zle autosuggest-start
#}
#zle -N zle-line-init

# https://github.com/zsh-users/zsh-syntax-highlighting

# https://github.com/zsh-users/zsh-history-substring-search
source ~/.oh-my-zsh/custom/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh
zmodload zsh/terminfo
bindkey "$terminfo[kcuu1]" history-substring-search-up
bindkey "$terminfo[kcud1]" history-substring-search-down

# stuff :>
alias l='ls --ignore="*.pyc"'
alias la='ls -A'
alias ll='ls -lh --ignore="*.pyc"'
alias lla='ls -lAh'
alias lt='ls -lrth --ignore="*.pyc"'
alias lat='ls -lrtAh'
alias temp='cat /sys/class/thermal/thermal_zone(0|1)/temp'
alias wp='feh --bg-fill'
alias speedtest='wget -O /dev/null http://speedtest.wdc01.softlayer.com/downloads/test100.zip'
export RPROMPT='%?'
function findpkg() {
    aura -Ss $1
    aura -As $1
}


# export MANPATH="/usr/local/man:$MANPATH"
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# export ARCHFLAGS="-arch x86_64"
# export SSH_KEY_PATH="~/.ssh/dsa_id"

# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# fortune
